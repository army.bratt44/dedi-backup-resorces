Mercedes Benz E-Class Ambulance (New UV map, non mirrored)

Status: Done
Version: 2.0
Replaces: ambulance

PLEASE NOTICE! There are a few skin bugs in the model. My apologies!

To follow updates on this model like: https://www.facebook.com/WorldMC1/

++++++++++++++++++++++++++++++++++++++++ 

Installation instruction: 

Extract the vehicle .ytd's + yft's to your mods folder; 
Folder -> x64e.rpf -> levels -> gta5 -> vehicles.rpf 

Visual settings:
GTAV/Mods/Update/Update.rpf/Common/Data


Credits: 

Model: TubeItGaming 
Blue LED's: Mohaalsmeer
UV-Map and template: World MC  - Mitchel
Skin: World MC  - Mitchel

++++++++++++++++++++++++++++++++++++++++ 

DO NOT REDISTRUBATE THIS MOD WITHOUT PERMISSION OF THOSE MENTIONED ABOVE! 

Contact us: worldcommunitymodding@gmail.com